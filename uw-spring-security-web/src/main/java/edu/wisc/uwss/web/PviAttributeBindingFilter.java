package edu.wisc.uwss.web;

import edu.wisc.uwss.UWUserDetails;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.filter.GenericFilterBean;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Filter to a UW user's PVI to a request. This sets an attribute named "wiscedupvi" to the value
 * of the currently authenticated user.
 */
public class PviAttributeBindingFilter extends GenericFilterBean {

    private static final Logger logger = LoggerFactory.getLogger(PviAttributeBindingFilter.class);

    /**
     * Add an attribute to the request with the currently authenticated user's PVI.
     *
     * @param servletRequest the request
     * @param servletResponse the response
     * @param filterChain the filter chain
     * @throws IOException
     * @throws ServletException
     */
    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest req = (HttpServletRequest)servletRequest;
        HttpServletResponse res = (HttpServletResponse)servletResponse;

        Authentication auth = SecurityContextHolder.getContext().getAuthentication();

        if (auth != null) {
            Object principal = auth.getPrincipal();
            if (principal instanceof UWUserDetails) {
                UWUserDetails userDetails = (UWUserDetails)principal;
                logger.debug("adding PVI attribute with value {} to request", userDetails.getPvi());
                req.setAttribute("wiscedupvi", userDetails.getPvi());
            }
        }

        filterChain.doFilter(req, res);
    }

}
