package edu.wisc.uwss.uds;

import org.junit.Test;

import edu.wisc.services.uds.person.v1_1.Identifier;
import edu.wisc.services.uds.person.v1_1.Identifiers;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

/**
 * Unit tests for {@link IdentifiersBuilder}.
 *
 * Examples provided from uds-person-client-java:
 *
 <pre>
 <uds:Identifier>
 <uds:Source>UWHRS</uds:Source>
 <uds:IdName>EMPLID</uds:IdName>
 <uds:Value>00000123</uds:Value>
 </uds:Identifier>
 <uds:Identifier>
 <uds:Source>UWMSNSUDS</uds:Source>
 <uds:IdName>NETID</uds:IdName>
 <uds:Value>bbadger</uds:Value>
 </uds:Identifier>
 <uds:Identifier>
 <uds:Source>UWMSNSUDS</uds:Source>
 <uds:IdName>PHOTOID</uds:IdName>
 <uds:Value>90212345671</uds:Value>
 </uds:Identifier>
 <uds:Identifier>
 <uds:Source>UWMSNSUDS</uds:Source>
 <uds:IdName>PVI</uds:IdName>
 <uds:Value>UW111Z000</uds:Value>
 </uds:Identifier>
 </pre>
 *
 * @author Nicholas Blair
 */
public class IdentifiersBuilderTest {

  /**
   * Confirm stable behavior when no identifiers added.
   */
  @Test
  public void toIdentifiers_empty() {
    assertEquals(new Identifiers(), new IdentifiersBuilder().toIdentifiers());
  }
  /**
   * Confirm behavior of withNetid.
   */
  @Test
  public void toIdentifiers_netid() {
    Identifiers identifiers = new IdentifiersBuilder().withNetid("bbadger").toIdentifiers();
    assertFalse(identifiers.getIdentifiers().isEmpty());
    Identifier id = identifiers.getIdentifiers().get(0);
    assertIdentifier(id, IdentifiersBuilder.NETID, IdentifiersBuilder.UWMSNSUDS, "bbadger");

  }
  /**
   * Confirm behavior of withPvi.
   */
  @Test
  public void toIdentifiers_pvi() {
    Identifiers identifiers = new IdentifiersBuilder().withPvi("UW111Z000").toIdentifiers();
    assertFalse(identifiers.getIdentifiers().isEmpty());
    Identifier id = identifiers.getIdentifiers().get(0);
    assertIdentifier(id, IdentifiersBuilder.PVI, IdentifiersBuilder.UWMSNSUDS, "UW111Z000");
  }

  /**
   * Confirm we can add multiple identifiers.
   */
  @Test
  public void toIdentifiers_both() {
    Identifiers identifiers = new IdentifiersBuilder()
            .withNetid("bbadger").withPvi("UW111Z000").toIdentifiers();
    assertFalse(identifiers.getIdentifiers().isEmpty());
    Identifier id = identifiers.getIdentifiers().get(0);
    assertIdentifier(identifiers.getIdentifiers().get(0), IdentifiersBuilder.NETID, IdentifiersBuilder.UWMSNSUDS, "bbadger");
    assertIdentifier(identifiers.getIdentifiers().get(1), IdentifiersBuilder.PVI, IdentifiersBuilder.UWMSNSUDS, "UW111Z000");
  }
  protected void assertIdentifier(Identifier id, String idName, String source, String value) {
    assertEquals(idName, id.getIdName());
    assertEquals(source, id.getSource());
    assertEquals(value, id.getValue());
  }
}
