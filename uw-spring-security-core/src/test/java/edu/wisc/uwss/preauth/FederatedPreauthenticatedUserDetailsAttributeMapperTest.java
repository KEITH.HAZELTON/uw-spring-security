package edu.wisc.uwss.preauth;

import edu.wisc.uwss.UWUserDetails;
import org.junit.Test;
import org.springframework.mock.web.MockHttpServletRequest;

import javax.servlet.http.HttpServletRequest;
import java.net.URL;
import java.util.Collections;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

/**
 * Tests for {@link FederatedPreauthenticatedUserDetailsAttributeMapper}.
 *
 * @author Benjamin Sousa
 */
public class FederatedPreauthenticatedUserDetailsAttributeMapperTest {

    private FederatedPreauthenticatedUserDetailsAttributeMapper federatedFilter = new FederatedPreauthenticatedUserDetailsAttributeMapper();

    /**
     * Verify expected behaviour for {@link FederatedPreauthenticatedUserDetailsAttributeMapper#mapUser(HttpServletRequest)}
     */
    @Test
    public void mapFederatedUser_success() {
        MockHttpServletRequest request = new MockHttpServletRequest();
        String eppn = "somebody@wisc.edu";
        String pvi = "1234567";
        String name = "some body";
        String email = "some.body@wisc.edu";
        List<String> uddsMembership = Collections.singletonList("udds1234");

        request.addHeader("eppn", eppn);
        request.addHeader("eduWisconsinSPVI", pvi);
        request.addHeader("cn", name);
        request.addHeader("mail", email);
        request.addHeader("eduWisconsinUDDS", uddsMembership);
        request.addHeader("Shib-Identity-Provider", "https://logintest.wisc.edu/idp/shibboleth");

        UWUserDetails result = federatedFilter.mapUser(request);

        assertNotNull(result);
        assertEquals(eppn, result.getEppn());
        assertEquals(pvi, result.getPvi());
        assertEquals(name, result.getFullName());
        assertEquals(email, result.getEmailAddress());
        assertEquals(uddsMembership, result.getUddsMembership());
        assertEquals("/Shibboleth.sso/Logout?return=https://logintest.wisc.edu/logout/", result.getCustomLogoutUrl());
    }

    /**
     * Verify behavior of {@link FederatedPreauthenticatedUserDetailsAttributeMapper#toCustomLogoutUrl(String)} for
     * null input.
     */
    @Test
    public void inputNullLogoutUrl() {
        assertNull(federatedFilter.toCustomLogoutUrl(null));
    }
    /**
     * Verify behavior of {@link FederatedPreauthenticatedUserDetailsAttributeMapper#toCustomLogoutUrl(String)} for
     * empty input.
     */
    @Test
    public void inputEmptyLogoutUrl() {
        assertNull(federatedFilter.toCustomLogoutUrl(""));
    }
    /**
     * Verify behavior of {@link FederatedPreauthenticatedUserDetailsAttributeMapper#toCustomLogoutUrl(String)} for
     * invalid input.
     */
    @Test
    public void inputInvalidLogoutUrl() {
        assertNull(federatedFilter.toCustomLogoutUrl("foo"));
    }
    /**
     * Verify behavior of {@link FederatedPreauthenticatedUserDetailsAttributeMapper#toCustomLogoutUrl(String)} for
     * a valid input with no {@link URL#getPath()}.
     */
    @Test
    public void omitLogoutPath() {
        assertEquals("/Shibboleth.sso/Logout?return=https://somewhere.wisc.edu/logout/", federatedFilter.toCustomLogoutUrl("https://somewhere.wisc.edu"));
    }
    /**
     * Verify behavior of {@link FederatedPreauthenticatedUserDetailsAttributeMapper#toCustomLogoutUrl(String)} for
     * expected valid input.
     */
    @Test
    public void inputValidLogoutUrl() {
        assertEquals("/Shibboleth.sso/Logout?return=https://somewhere.wisc.edu/logout/", federatedFilter.toCustomLogoutUrl("https://somewhere.wisc.edu/idp/shibboleth"));
    }
    /**
     * Verify behavior of {@link FederatedPreauthenticatedUserDetailsAttributeMapper#toCustomLogoutUrl(String)} for
     * expected valid input and a different customLogoutPrefix.
     */
    @Test
    public void appendCustomLogoutPrefix() {
        federatedFilter.setCustomLogoutPrefix("/Chibbolath.sso/Logout?return=");
        assertEquals("/Chibbolath.sso/Logout?return=https://somewhere.wisc.edu/logout/", federatedFilter.toCustomLogoutUrl("https://somewhere.wisc.edu/idp/shibboleth"));
    }
    /**
     * Verify behavior of {@link FederatedPreauthenticatedUserDetailsAttributeMapper#toCustomLogoutUrl(String)} for
     * expected valid input and a different customLogoutSuffix.
     */
    @Test
    public void appendCustomLogoutSuffix() {
        federatedFilter.setCustomLogoutSuffix("/luguot/");
        assertEquals("/Shibboleth.sso/Logout?return=https://somewhere.wisc.edu/luguot/", federatedFilter.toCustomLogoutUrl("https://somewhere.wisc.edu/idp/shibboleth"));
    }

}
