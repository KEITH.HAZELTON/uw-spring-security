/**
 *
 */
package edu.wisc.uwss.local;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import edu.wisc.services.uds.person.v1_1.Appointment;
import edu.wisc.services.uds.person.v1_1.Appointments;
import edu.wisc.services.uds.person.v1_1.Demographic;
import edu.wisc.services.uds.person.v1_1.Employee;
import edu.wisc.services.uds.person.v1_1.Identifier;
import edu.wisc.services.uds.person.v1_1.Identifiers;
import edu.wisc.services.uds.person.v1_1.Name;
import edu.wisc.services.uds.person.v1_1.Person;
import edu.wisc.services.uds.person.v1_1.UDDS;
import edu.wisc.uds.UdsPersonService;
import edu.wisc.uwss.UWUserDetails;

/**
 * Implementation of {@link UdsPersonService} that is intended for use
 * with the local development "local-users" {@link Profile}.
 *
 * @author Nicholas Blair
 */
@Profile({ "local-users", "edu.wisc.uwss.local-users" })
public class LocalUsersUdsPersonServiceImpl implements UdsPersonService {

  private final Logger logger = LoggerFactory.getLogger(getClass());
  @Autowired
  private UserDetailsService userDetailsService;
  /* (non-Javadoc)
   * @see edu.wisc.uds.UdsPersonService#getPerson(edu.wisc.services.uds.person.v1_1.Identifiers)
   */
  @Override
  public Person getPerson(Identifiers identifiers) {
    for(Identifier identifier: identifiers.getIdentifiers()) {
      try {
        UWUserDetails userDetails = (UWUserDetails) userDetailsService.loadUserByUsername(identifier.getValue());

        // method won't return null by contract, it'll throw UsernameNotFoundException instead
        Person result = new Person();
        Demographic demographic = new Demographic();
        demographic.setEmail(userDetails.getEmailAddress());
        Name name = new Name();
        name.setFirst(userDetails.getFirstName());
        name.setLast(userDetails.getLastName());
        name.setFull(userDetails.getFullName());
        demographic.setName(name);

        result.setDemographic(demographic);
        Employee employee = new Employee();
        employee.setAppointments(new Appointments());
        for(String udds : userDetails.getUddsMembership()) {
          Appointment appointment = new Appointment();
          UDDS u = new UDDS();
          u.setCode(udds);
          appointment.setUDDS(u);
          employee.getAppointments().getAppointments().add(appointment);
        }

        result.setEmployee(employee);
        return result;
      } catch (UsernameNotFoundException e) {
        logger.debug("no userDetails found for " + identifier, e);
      }
    }
    return null;
  }

  /* (non-Javadoc)
   * @see edu.wisc.uds.UdsPersonService#getPeople(java.util.List)
   */
  @Override
  public List<Person> getPeople(List<Identifiers> identifiers) {
    throw new UnsupportedOperationException("source to implement getPeople(List) not provided via Spring's UserDetailsService");
  }

}
