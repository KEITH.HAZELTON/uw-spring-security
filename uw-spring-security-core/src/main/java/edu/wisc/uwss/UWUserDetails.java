/**
 * 
 */
package edu.wisc.uwss;

import java.util.Collection;

import org.springframework.security.core.userdetails.AuthenticationUserDetailsService;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;

/**
 * UW specific extension of {@link UserDetails}, Spring Security's core model for a user in your
 * application.
 * 
 * The intent for this interface is to provide means for retrieving commonly used user attributes
 * beyond what {@link UserDetails} provides.
 * 
 * @author Nicholas Blair
 */
public interface UWUserDetails extends UserDetails {

  /**
   * "eppn" is the scoped username and typically (NOT ALWAYS) takes the form 'netid@wisc.edu'
   * This method will return null if the {@link UserDetailsService} backing it is incapable of providing an eppn.
   * 
   * @see https://www.incommon.org/federation/attributesummary.html
   * @return the eppn for this user, or null.
   */
  String getEppn();

  /**
   * PVI stands for "publicly visible identifier" and is intended to serve as the entity identifier
   * for a user. This attribute is the preferred choice for unique identifier. While it can change
   * for a given individual, it is least likely of the identifying attributes to change, and only
   * changes in extreme edge cases.
   * 
   * Implementations should not return null if possible.
   * 
   * @return the pvi for this user
   */
  String getPvi();

  /**
   * 
   * @return the full name for this user, or null if not available
   */
  String getFullName();

  /**
   * @return a never null, but potentially empty, {@link Collection} of {@link String}s each
   *         representing the UDDS Ids of the groups this user is a member of.
   */
  Collection<String> getUddsMembership();

  /**
   * Implementations should generally prefer to return email addresses in lower case.
   * 
   * @return the emailAddress for this user, or null if not available
   */
  String getEmailAddress();

  /**
   * ID representing the source system that provided this user. Implementations of
   * {@link UserDetailsService} or {@link AuthenticationUserDetailsService} will typically populate
   * this field for the concrete {@link UWUserDetails} they return.
   * 
   * @return the source (never null).
   */
  String getSource();

  /**
   * Some {@link #getSource()}s (like "preauth") need a custom "log out" location to be sent to.
   * This method provides a custom log out url if needed for this type of user.
   * 
   * @return a custom "log out" url for this user, or null
   */
  String getCustomLogoutUrl();
  
  /**
   * "ISIS emplid" is a unique identifier for a person within the Peoplesoft student information system.
   * This method may return null.
   * 
   * @return the ISIS emplid for this user, or null
   */
  String getIsisEmplid();
  
  /**
   * This is typically not a computed field.
   * 
   * @return the person's first name, or null if not available
   */
  String getFirstName();
  /**
   * This is typically not a computed field.
   * 
   * @return the person's last name, or null if not available
   */
  String getLastName();
}
