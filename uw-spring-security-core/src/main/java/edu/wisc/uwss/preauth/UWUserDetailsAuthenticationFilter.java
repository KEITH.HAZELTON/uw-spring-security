/**
 * 
 */
package edu.wisc.uwss.preauth;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.web.authentication.preauth.AbstractPreAuthenticatedProcessingFilter;

import edu.wisc.uwss.UWUserDetails;
import edu.wisc.uwss.UWUserDetailsImpl;

/**
 * Subclass of {@link AbstractPreAuthenticatedProcessingFilter} that returns {@link UWUserDetailsImpl}
 * for {@link #getPreAuthenticatedPrincipal(HttpServletRequest)}.
 * 
 * @author Nicholas Blair
 */
public class UWUserDetailsAuthenticationFilter extends
AbstractPreAuthenticatedProcessingFilter {

  @Autowired(required=false)
  private PreauthenticatedUserDetailsAttributeMapper userDetailsAttributeMapper = new PreauthenticatedUserDetailsAttributeMapper.Default();

  /**
   * {@inheritDoc}
   * 
   * Returns a {@link UWUserDetails} from the attributes provided via request headers. Returns null
   * if the usernameHeader is not present.
   */
  @Override
  protected UWUserDetails getPreAuthenticatedPrincipal(HttpServletRequest request) {
    return userDetailsAttributeMapper.mapUser(request);
  }
  /**
   * {@inheritDoc}
   * 
   * Returns an empty string as credentials (read: the password) are not available in our usage of Shibboleth.
   */
  @Override
  protected Object getPreAuthenticatedCredentials(HttpServletRequest request) {
    return "";
  }
}
