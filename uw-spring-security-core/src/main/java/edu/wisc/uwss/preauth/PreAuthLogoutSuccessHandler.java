/**
 * 
 */
package edu.wisc.uwss.preauth;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.logout.SimpleUrlLogoutSuccessHandler;

import edu.wisc.uwss.UWUserDetails;

/**
 * Subclass of {@link SimpleUrlLogoutSuccessHandler} that will redirect to {@link UWUserDetails#getCustomLogoutUrl()}
 * if set. Defaults to super's behavior if not.
 * 
 * @author Nicholas Blair
 */
public class PreAuthLogoutSuccessHandler extends SimpleUrlLogoutSuccessHandler {
  
  /* (non-Javadoc)
   * @see org.springframework.security.web.authentication.logout.LogoutSuccessHandler#onLogoutSuccess(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse, org.springframework.security.core.Authentication)
   */
  @Override
  public void onLogoutSuccess(HttpServletRequest request, HttpServletResponse response,
      Authentication authentication) throws IOException, ServletException {
    if(authentication == null || authentication.getPrincipal() == null) {
      super.onLogoutSuccess(request, response, authentication);
    } else {
      String customLogoutUrl = ((UWUserDetails) authentication.getPrincipal()).getCustomLogoutUrl();
      if(customLogoutUrl == null) {
        super.onLogoutSuccess(request, response, authentication);
      } else {
        // intentionally not using DefaultRedirectStrategy.
        // even with contextRelative=false, DefaultRedirectStrategy will see our customLogoutUrl doesn't have
        // a URI scheme/host/port, and treat it as if it should, which in turn injects the context into the path
        response.sendRedirect(customLogoutUrl);
      }
    }
  }

  
}
