package edu.wisc.uwss.configuration.preauth;

import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.web.authentication.preauth.AbstractPreAuthenticatedProcessingFilter;

import edu.wisc.uwss.configuration.HttpSecurityAmender;
import edu.wisc.uwss.preauth.PreAuthLogoutSuccessHandler;
import edu.wisc.uwss.preauth.UWUserDetailsAuthenticationFilter;

/**
 * {@link HttpSecurityAmender} intended to inject the appropriate filter and handler to
 * support "preauthentication". This is typically used at UW to support NetID Login (SAML2 over Shibboleth).
 *
 * @author Nicholas Blair
 */
public class PreAuthenticationHttpSecurityAmender implements HttpSecurityAmender {
  private final UWUserDetailsAuthenticationFilter authenticationFilter;
  public PreAuthenticationHttpSecurityAmender(UWUserDetailsAuthenticationFilter authenticationFilter) {
    this.authenticationFilter = authenticationFilter;
  }
  /**
   * {@inheritDoc}
   *
   * <ul>
   * <li>Adds {@link UWUserDetailsAuthenticationFilter} to the chain.</li>
   * <li>Wires in {@link PreAuthLogoutSuccessHandler} to logout.</li>
   * <li>Disables CSRF.</li>
   * </ul>
   */
  @Override
  public void amend(HttpSecurity http) throws Exception {
    http
            .addFilterBefore(authenticationFilter, AbstractPreAuthenticatedProcessingFilter.class)
            .logout()
            .logoutSuccessHandler(new PreAuthLogoutSuccessHandler())
            .deleteCookies("JSESSIONID")
            .invalidateHttpSession(true);

    // CSRF protection is disabled, as without it /logout requires POST
    http
            .csrf().disable();
  }
}
