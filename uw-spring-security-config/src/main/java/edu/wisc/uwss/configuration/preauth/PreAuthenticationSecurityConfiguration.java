/**
 * 
 */
package edu.wisc.uwss.configuration.preauth;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.method.configuration.GlobalMethodSecurityConfiguration;
import org.springframework.security.core.userdetails.AuthenticationUserDetailsService;
import org.springframework.security.web.authentication.preauth.PreAuthenticatedAuthenticationProvider;
import org.springframework.security.web.authentication.preauth.PreAuthenticatedAuthenticationToken;

import edu.wisc.uwss.preauth.FederatedPreauthenticatedUserDetailsAttributeMapper;
import edu.wisc.uwss.preauth.UWUserDetailsAuthenticationUserDetailsService;

/**
 * Isolate Pre-Authentication (Shibboleth) configuration.
 * 
 * Activated with the "preauth" profile.
 * 
 * @author Nicholas Blair
 */
@Configuration
@Profile({ "preauth", "edu.wisc.uwss.preauth" })
@EnableGlobalMethodSecurity(prePostEnabled=true)
public class PreAuthenticationSecurityConfiguration extends GlobalMethodSecurityConfiguration {

  final static Logger logger = LoggerFactory.getLogger(PreAuthenticationSecurityConfiguration.class);
  /**
   * {@inheritDoc}
   * 
   * Injects {@link #preAuthenticationProvider(AuthenticationUserDetailsService)} into the {@link AuthenticationManagerBuilder}.
   * 
   * @see AuthenticationManagerBuilder#authenticationProvider(AuthenticationProvider)
   * @param auth
   * @throws Exception
   */
  @Override
  public void configure(AuthenticationManagerBuilder auth) throws Exception {
    auth.authenticationProvider(preAuthenticationProvider(preAuthenticationUserDetailsService()));
    logger.warn("injected pre-authentication authenticationProvider");
  }
  /**
   * 
   * @param userDetailsService the required {@link AuthenticationUserDetailsService}
   * @return a {@link PreAuthenticatedAuthenticationProvider}
   */
  @Bean @Autowired
  public AuthenticationProvider preAuthenticationProvider(AuthenticationUserDetailsService<PreAuthenticatedAuthenticationToken> userDetailsService) {
    PreAuthenticatedAuthenticationProvider provider = new PreAuthenticatedAuthenticationProvider();
    provider.setPreAuthenticatedUserDetailsService(userDetailsService);
    return provider;
  }
  /**
   * Returns our custom {@link AuthenticationUserDetailsService}.
   * 
   * @return an instance of {@link UWUserDetailsAuthenticationUserDetailsService}.
   */
  @Bean
  public AuthenticationUserDetailsService<PreAuthenticatedAuthenticationToken> preAuthenticationUserDetailsService() {
    return new UWUserDetailsAuthenticationUserDetailsService();
  }
  /**
   * If the Spring {@link Profile} "edu.wisc.uwss.preauth.federation" is active, register
   * an instance of {@link FederatedPreauthenticatedUserDetailsAttributeMapper}.
   *
   * @return an instance of {@link FederatedPreauthenticatedUserDetailsAttributeMapper}
   */
  @Bean @Profile("edu.wisc.uwss.preauth.federation")
  public FederatedPreauthenticatedUserDetailsAttributeMapper federationAttributeMapper() {
    return new FederatedPreauthenticatedUserDetailsAttributeMapper();
  }

  /**
   * {@inheritDoc}
   * 
   * Exposed as a {@link Bean}.
   */
  @Bean
  protected AuthenticationManager authenticationManager() throws Exception {
    return super.authenticationManager();
  }

}
