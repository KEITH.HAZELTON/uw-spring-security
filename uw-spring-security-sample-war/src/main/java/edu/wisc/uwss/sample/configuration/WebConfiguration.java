/**
 * 
 */
package edu.wisc.uwss.sample.configuration;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

/**
 * 
 * @author Nicholas Blair
 */
@Configuration
@EnableWebMvc
@ComponentScan({"edu.wisc.uwss.web", "edu.wisc.uwss.sample.web"})
public class WebConfiguration extends WebMvcConfigurerAdapter {

  /* (non-Javadoc)
   * @see org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter#addResourceHandlers(org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry)
   */
  @Override
  public void addResourceHandlers(ResourceHandlerRegistry registry) {
    registry.addResourceHandler("/*").addResourceLocations("/");
  }

}
